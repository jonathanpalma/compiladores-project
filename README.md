# Proyecto de catedra - Compiladores

### Autores 

  - William Alvarado
  - Jonathan Palma
  - Alejandro Paz

### Ejemplos

- Calculo de area de un triangulo
```c
int base = 5;
int altura = 7;
float area = (base * altura) / 2;
```

- Contador usando while
```c
int main () {
   int i = 0;
   while( i < 100 ) {
      i++;
   }
   return 0;
}
```

- Calculo de numeros primos usando estructuras anidadas
```c
int main()
{
  int i;
  int j;
  int primos = 0;
  for (i = 2; i < 100; i++)
  {
    for (j = 2; j <= (i / j); j++)
    {
      if (!(i % j))
      {
        break; // si el numero no es primo
      }
      if (j > (i / j))
      {
        primos++;
      }
    }
  }
  return 0;
}
```

- Suma de los primeros 20 numeros pares usando for
```c
int main()
{
  int a = 0;
  int b = 0;
  for (b = 2; b <= 40; b++)
  {
    if (b % 2 == 0)
    {
      a = a + b;
    }
  }
  return 0;
}
```