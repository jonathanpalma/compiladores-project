int main()
{
  int i;
  int j;
  int primos = 0;

  for (i = 2; i < 100; i++)
  {
    for (j = 2; j <= (i / j); j++)
    {
      if (!(i % j))
      {
        break; // si el numero no es primo
      }
      if (j > (i / j))
      {
        primos++;
      }
    }
  }
  return 0;
}