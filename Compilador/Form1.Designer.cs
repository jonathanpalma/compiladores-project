﻿

namespace Compilador
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tablaSimbolos = new System.Windows.Forms.DataGridView();
            this.Codigo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Variable = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tipo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Valor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cuadroCodigoIntermedio = new System.Windows.Forms.TextBox();
            this.cuadroSintactico = new System.Windows.Forms.TextBox();
            this.cuadroLexico = new System.Windows.Forms.TextBox();
            this.cuadroSemantico = new System.Windows.Forms.TextBox();
            this.cuadroTexto = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.archivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.abrirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guardarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.limpiarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.analizadoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lexicoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.sintacticoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.semanticoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generadorToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.codigoIntermedioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.codigoMaquinaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ayudaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.acercaDeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.arbolSintax = new System.Windows.Forms.TreeView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.gbAnalizadores = new System.Windows.Forms.GroupBox();
            this.lblSemantico = new System.Windows.Forms.Label();
            this.lblSintactico = new System.Windows.Forms.Label();
            this.lblLexico = new System.Windows.Forms.Label();
            this.gbResultados = new System.Windows.Forms.GroupBox();
            this.gbEntrada = new System.Windows.Forms.GroupBox();
            this.gbGenerador = new System.Windows.Forms.GroupBox();
            this.compilarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.tablaSimbolos)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.gbAnalizadores.SuspendLayout();
            this.gbResultados.SuspendLayout();
            this.gbEntrada.SuspendLayout();
            this.gbGenerador.SuspendLayout();
            this.SuspendLayout();
            // 
            // tablaSimbolos
            // 
            this.tablaSimbolos.AllowUserToAddRows = false;
            this.tablaSimbolos.AllowUserToDeleteRows = false;
            this.tablaSimbolos.BackgroundColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tablaSimbolos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tablaSimbolos.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo,
            this.Variable,
            this.Tipo,
            this.Valor});
            this.tablaSimbolos.Location = new System.Drawing.Point(13, 24);
            this.tablaSimbolos.Name = "tablaSimbolos";
            this.tablaSimbolos.ReadOnly = true;
            this.tablaSimbolos.Size = new System.Drawing.Size(444, 163);
            this.tablaSimbolos.TabIndex = 0;
            // 
            // Codigo
            // 
            this.Codigo.HeaderText = "Codigo";
            this.Codigo.Name = "Codigo";
            this.Codigo.ReadOnly = true;
            // 
            // Variable
            // 
            this.Variable.HeaderText = "Variable";
            this.Variable.Name = "Variable";
            this.Variable.ReadOnly = true;
            // 
            // Tipo
            // 
            this.Tipo.HeaderText = "Tipo";
            this.Tipo.Name = "Tipo";
            this.Tipo.ReadOnly = true;
            // 
            // Valor
            // 
            this.Valor.HeaderText = "Valor";
            this.Valor.Name = "Valor";
            this.Valor.ReadOnly = true;
            // 
            // cuadroCodigoIntermedio
            // 
            this.cuadroCodigoIntermedio.AcceptsReturn = true;
            this.cuadroCodigoIntermedio.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cuadroCodigoIntermedio.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.cuadroCodigoIntermedio.Location = new System.Drawing.Point(13, 24);
            this.cuadroCodigoIntermedio.Multiline = true;
            this.cuadroCodigoIntermedio.Name = "cuadroCodigoIntermedio";
            this.cuadroCodigoIntermedio.ReadOnly = true;
            this.cuadroCodigoIntermedio.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.cuadroCodigoIntermedio.Size = new System.Drawing.Size(373, 163);
            this.cuadroCodigoIntermedio.TabIndex = 1;
            // 
            // cuadroSintactico
            // 
            this.cuadroSintactico.AcceptsReturn = true;
            this.cuadroSintactico.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cuadroSintactico.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cuadroSintactico.Location = new System.Drawing.Point(296, 36);
            this.cuadroSintactico.Multiline = true;
            this.cuadroSintactico.Name = "cuadroSintactico";
            this.cuadroSintactico.ReadOnly = true;
            this.cuadroSintactico.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.cuadroSintactico.Size = new System.Drawing.Size(283, 123);
            this.cuadroSintactico.TabIndex = 3;
            // 
            // cuadroLexico
            // 
            this.cuadroLexico.AcceptsReturn = true;
            this.cuadroLexico.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cuadroLexico.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cuadroLexico.Location = new System.Drawing.Point(10, 36);
            this.cuadroLexico.Multiline = true;
            this.cuadroLexico.Name = "cuadroLexico";
            this.cuadroLexico.ReadOnly = true;
            this.cuadroLexico.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.cuadroLexico.Size = new System.Drawing.Size(283, 123);
            this.cuadroLexico.TabIndex = 1;
            this.cuadroLexico.WordWrap = false;
            // 
            // cuadroSemantico
            // 
            this.cuadroSemantico.AcceptsReturn = true;
            this.cuadroSemantico.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cuadroSemantico.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cuadroSemantico.Location = new System.Drawing.Point(582, 36);
            this.cuadroSemantico.Multiline = true;
            this.cuadroSemantico.Name = "cuadroSemantico";
            this.cuadroSemantico.ReadOnly = true;
            this.cuadroSemantico.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.cuadroSemantico.Size = new System.Drawing.Size(283, 123);
            this.cuadroSemantico.TabIndex = 5;
            // 
            // cuadroTexto
            // 
            this.cuadroTexto.AcceptsReturn = true;
            this.cuadroTexto.BackColor = System.Drawing.SystemColors.Window;
            this.cuadroTexto.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cuadroTexto.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cuadroTexto.Location = new System.Drawing.Point(9, 19);
            this.cuadroTexto.Multiline = true;
            this.cuadroTexto.Name = "cuadroTexto";
            this.cuadroTexto.Size = new System.Drawing.Size(856, 144);
            this.cuadroTexto.TabIndex = 0;
            this.cuadroTexto.WordWrap = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.archivoToolStripMenuItem,
            this.compilarToolStripMenuItem,
            this.analizadoresToolStripMenuItem,
            this.generadorToolStripMenuItem,
            this.ayudaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(893, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // archivoToolStripMenuItem
            // 
            this.archivoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevoToolStripMenuItem,
            this.abrirToolStripMenuItem,
            this.guardarToolStripMenuItem,
            this.limpiarToolStripMenuItem});
            this.archivoToolStripMenuItem.Name = "archivoToolStripMenuItem";
            this.archivoToolStripMenuItem.Size = new System.Drawing.Size(60, 20);
            this.archivoToolStripMenuItem.Text = "&Archivo";
            // 
            // nuevoToolStripMenuItem
            // 
            this.nuevoToolStripMenuItem.Name = "nuevoToolStripMenuItem";
            this.nuevoToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.nuevoToolStripMenuItem.Text = "&Nuevo";
            this.nuevoToolStripMenuItem.Click += new System.EventHandler(this.NuevoToolStripMenuItem_Click);
            // 
            // abrirToolStripMenuItem
            // 
            this.abrirToolStripMenuItem.Name = "abrirToolStripMenuItem";
            this.abrirToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.abrirToolStripMenuItem.Text = "&Abrir";
            this.abrirToolStripMenuItem.Click += new System.EventHandler(this.AbrirToolStripMenuItem_Click);
            // 
            // guardarToolStripMenuItem
            // 
            this.guardarToolStripMenuItem.Name = "guardarToolStripMenuItem";
            this.guardarToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.guardarToolStripMenuItem.Text = "&Guardar";
            this.guardarToolStripMenuItem.Click += new System.EventHandler(this.GuardarToolStripMenuItem_Click);
            // 
            // limpiarToolStripMenuItem
            // 
            this.limpiarToolStripMenuItem.Name = "limpiarToolStripMenuItem";
            this.limpiarToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.limpiarToolStripMenuItem.Text = "&Limpiar";
            this.limpiarToolStripMenuItem.Click += new System.EventHandler(this.LimpiarToolStripMenuItem_Click);
            // 
            // analizadoresToolStripMenuItem
            // 
            this.analizadoresToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lexicoToolStripMenuItem,
            this.sintacticoToolStripMenuItem,
            this.semanticoToolStripMenuItem});
            this.analizadoresToolStripMenuItem.Name = "analizadoresToolStripMenuItem";
            this.analizadoresToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.analizadoresToolStripMenuItem.Text = "&Analizador";
            // 
            // lexicoToolStripMenuItem
            // 
            this.lexicoToolStripMenuItem.Name = "lexicoToolStripMenuItem";
            this.lexicoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.lexicoToolStripMenuItem.Text = "&Lexico";
            this.lexicoToolStripMenuItem.Click += new System.EventHandler(this.LexicoToolStripMenuItem_Click);
            // 
            // sintacticoToolStripMenuItem
            // 
            this.sintacticoToolStripMenuItem.Name = "sintacticoToolStripMenuItem";
            this.sintacticoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.sintacticoToolStripMenuItem.Text = "&Sintactico";
            this.sintacticoToolStripMenuItem.Click += new System.EventHandler(this.SintacticoToolStripMenuItem_Click);
            // 
            // semanticoToolStripMenuItem
            // 
            this.semanticoToolStripMenuItem.Name = "semanticoToolStripMenuItem";
            this.semanticoToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.semanticoToolStripMenuItem.Text = "&Semantico";
            this.semanticoToolStripMenuItem.Click += new System.EventHandler(this.SemanticoToolStripMenuItem_Click);
            // 
            // generadorToolStripMenuItem
            // 
            this.generadorToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.codigoIntermedioToolStripMenuItem,
            this.codigoMaquinaToolStripMenuItem});
            this.generadorToolStripMenuItem.Name = "generadorToolStripMenuItem";
            this.generadorToolStripMenuItem.Size = new System.Drawing.Size(74, 20);
            this.generadorToolStripMenuItem.Text = "&Generador";
            // 
            // codigoIntermedioToolStripMenuItem
            // 
            this.codigoIntermedioToolStripMenuItem.Name = "codigoIntermedioToolStripMenuItem";
            this.codigoIntermedioToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.codigoIntermedioToolStripMenuItem.Text = "&Codigo Intermedio";
            this.codigoIntermedioToolStripMenuItem.Click += new System.EventHandler(this.CodigoIntermedioToolStripMenuItem_Click);
            // 
            // codigoMaquinaToolStripMenuItem
            // 
            this.codigoMaquinaToolStripMenuItem.Name = "codigoMaquinaToolStripMenuItem";
            this.codigoMaquinaToolStripMenuItem.Size = new System.Drawing.Size(174, 22);
            this.codigoMaquinaToolStripMenuItem.Text = "&Codigo Maquina";
            this.codigoMaquinaToolStripMenuItem.Click += new System.EventHandler(this.CodigoMaquinaToolStripMenuItem_Click);
            // 
            // ayudaToolStripMenuItem
            // 
            this.ayudaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.acercaDeToolStripMenuItem});
            this.ayudaToolStripMenuItem.Name = "ayudaToolStripMenuItem";
            this.ayudaToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.ayudaToolStripMenuItem.Text = "&Ayuda";
            // 
            // acercaDeToolStripMenuItem
            // 
            this.acercaDeToolStripMenuItem.Name = "acercaDeToolStripMenuItem";
            this.acercaDeToolStripMenuItem.Size = new System.Drawing.Size(179, 22);
            this.acercaDeToolStripMenuItem.Text = "&Acerca del proyecto";
            this.acercaDeToolStripMenuItem.Click += new System.EventHandler(this.AcercaDeToolStripMenuItem_Click);
            // 
            // arbolSintax
            // 
            this.arbolSintax.Location = new System.Drawing.Point(826, 21);
            this.arbolSintax.Name = "arbolSintax";
            this.arbolSintax.Size = new System.Drawing.Size(10, 10);
            this.arbolSintax.TabIndex = 1;
            this.arbolSintax.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.arbolSintax);
            this.panel2.Location = new System.Drawing.Point(0, 27);
            this.panel2.Name = "panel2";
            this.panel2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.panel2.Size = new System.Drawing.Size(10, 486);
            this.panel2.TabIndex = 0;
            // 
            // gbAnalizadores
            // 
            this.gbAnalizadores.Controls.Add(this.lblSemantico);
            this.gbAnalizadores.Controls.Add(this.lblSintactico);
            this.gbAnalizadores.Controls.Add(this.lblLexico);
            this.gbAnalizadores.Controls.Add(this.cuadroLexico);
            this.gbAnalizadores.Controls.Add(this.cuadroSemantico);
            this.gbAnalizadores.Controls.Add(this.cuadroSintactico);
            this.gbAnalizadores.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbAnalizadores.Location = new System.Drawing.Point(10, 207);
            this.gbAnalizadores.Name = "gbAnalizadores";
            this.gbAnalizadores.Size = new System.Drawing.Size(875, 172);
            this.gbAnalizadores.TabIndex = 2;
            this.gbAnalizadores.TabStop = false;
            this.gbAnalizadores.Text = "Analizadores";
            // 
            // lblSemantico
            // 
            this.lblSemantico.AutoSize = true;
            this.lblSemantico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSemantico.Location = new System.Drawing.Point(687, 16);
            this.lblSemantico.Name = "lblSemantico";
            this.lblSemantico.Size = new System.Drawing.Size(72, 16);
            this.lblSemantico.TabIndex = 4;
            this.lblSemantico.Text = "Semantico";
            // 
            // lblSintactico
            // 
            this.lblSintactico.AutoSize = true;
            this.lblSintactico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSintactico.Location = new System.Drawing.Point(404, 16);
            this.lblSintactico.Name = "lblSintactico";
            this.lblSintactico.Size = new System.Drawing.Size(66, 16);
            this.lblSintactico.TabIndex = 2;
            this.lblSintactico.Text = "Sintactico";
            // 
            // lblLexico
            // 
            this.lblLexico.AutoSize = true;
            this.lblLexico.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLexico.Location = new System.Drawing.Point(128, 17);
            this.lblLexico.Name = "lblLexico";
            this.lblLexico.Size = new System.Drawing.Size(47, 16);
            this.lblLexico.TabIndex = 0;
            this.lblLexico.Text = "Lexico";
            // 
            // gbResultados
            // 
            this.gbResultados.Controls.Add(this.tablaSimbolos);
            this.gbResultados.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbResultados.Location = new System.Drawing.Point(10, 385);
            this.gbResultados.Name = "gbResultados";
            this.gbResultados.Size = new System.Drawing.Size(470, 199);
            this.gbResultados.TabIndex = 3;
            this.gbResultados.TabStop = false;
            this.gbResultados.Text = "Resultados";
            // 
            // gbEntrada
            // 
            this.gbEntrada.Controls.Add(this.cuadroTexto);
            this.gbEntrada.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbEntrada.Location = new System.Drawing.Point(10, 28);
            this.gbEntrada.Name = "gbEntrada";
            this.gbEntrada.Size = new System.Drawing.Size(875, 173);
            this.gbEntrada.TabIndex = 1;
            this.gbEntrada.TabStop = false;
            this.gbEntrada.Text = "Entrada";
            // 
            // gbGenerador
            // 
            this.gbGenerador.Controls.Add(this.cuadroCodigoIntermedio);
            this.gbGenerador.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gbGenerador.Location = new System.Drawing.Point(489, 385);
            this.gbGenerador.Name = "gbGenerador";
            this.gbGenerador.Size = new System.Drawing.Size(396, 197);
            this.gbGenerador.TabIndex = 4;
            this.gbGenerador.TabStop = false;
            this.gbGenerador.Text = "Generador de codigo";
            // 
            // compilarToolStripMenuItem
            // 
            this.compilarToolStripMenuItem.Name = "compilarToolStripMenuItem";
            this.compilarToolStripMenuItem.Size = new System.Drawing.Size(68, 20);
            this.compilarToolStripMenuItem.Text = "&Compilar";
            this.compilarToolStripMenuItem.Click += new System.EventHandler(this.CompilarToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.ClientSize = new System.Drawing.Size(893, 594);
            this.Controls.Add(this.gbGenerador);
            this.Controls.Add(this.gbEntrada);
            this.Controls.Add(this.gbResultados);
            this.Controls.Add(this.gbAnalizadores);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Compilador";
            ((System.ComponentModel.ISupportInitialize)(this.tablaSimbolos)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.gbAnalizadores.ResumeLayout(false);
            this.gbAnalizadores.PerformLayout();
            this.gbResultados.ResumeLayout(false);
            this.gbEntrada.ResumeLayout(false);
            this.gbEntrada.PerformLayout();
            this.gbGenerador.ResumeLayout(false);
            this.gbGenerador.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.DataGridView tablaSimbolos;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Variable;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tipo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Valor;
        private System.Windows.Forms.TextBox cuadroCodigoIntermedio;
        private System.Windows.Forms.TextBox cuadroSemantico;
        private System.Windows.Forms.TextBox cuadroTexto;
        private System.Windows.Forms.TextBox cuadroLexico;
        private System.Windows.Forms.TextBox cuadroSintactico;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem archivoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem abrirToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guardarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem analizadoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem lexicoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem sintacticoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem semanticoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generadorToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem codigoIntermedioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem codigoMaquinaToolStripMenuItem;
        private System.Windows.Forms.TreeView arbolSintax;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox gbAnalizadores;
        private System.Windows.Forms.GroupBox gbResultados;
        private System.Windows.Forms.Label lblSemantico;
        private System.Windows.Forms.Label lblSintactico;
        private System.Windows.Forms.Label lblLexico;
        private System.Windows.Forms.GroupBox gbEntrada;
        private System.Windows.Forms.GroupBox gbGenerador;
        private System.Windows.Forms.ToolStripMenuItem ayudaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem acercaDeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem limpiarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem compilarToolStripMenuItem;
    }
}

