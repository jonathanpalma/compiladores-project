﻿using Compilador.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Compilador.Controllers
{
    class CodigoIntermedio
    {
        public TablaSimbolos Tabla { get; set; }


        public CodigoIntermedio()
        {
            Tabla = new TablaSimbolos();
        }

        public void ConstruirTabla(List<Lexema> lex)
        {
            Tabla.ProcesarListadoLexemas(lex);
        }

        public List<String> Genera_codigo(List<Lexema> lex)
        {
            List<String> codigo = new List<String>();
            Lexema[] temp = lex.ToArray<Lexema>();
            String[] variable = new String[ObtenerTotVariables(temp)];
            int contador = 0;
            int bandera = 0;
            int t = 1;
            for (int i = 0; i < temp.Length; i++)
            {
                if (temp[i].TipoElemento == Enums.TipoElemento.Variable)
                {
                    contador++;
                    variable[bandera] = temp[i].Texto;
                    String cadena = "t" + (t) + " := " + temp[i].Texto + "\r\n";
                    codigo.Add(cadena);
                    bandera++;
                    t++;
                }
                if (temp[i].TipoElemento == Enums.TipoElemento.OperadorAsignacion)
                {
                    if (Verificar(ref variable, ref temp))
                    {
                        String ti = "t" + ObtenerIndice(variable, temp[i - 1].Texto);
                        String cadena = ti + " := " + temp[i + 1].Texto + "\r\n";
                        codigo.Add(cadena);
                        contador++;
                    }
                }
                if (temp[i].TipoElemento == Enums.TipoElemento.OperadorAritmetico)
                {
                    if (Verificar(ref variable, ref temp))
                    {
                        contador++;

                        String tn = "t" + (contador - ObtenerIndice(variable, temp[i - 1].Texto));
                        String tm = "t" + ObtenerIndice(variable, temp[i - 1].Texto);
                        String tp = "";
                        String cadena = "";
                        if (temp[i + 1].TipoElemento != Enums.TipoElemento.Numero)
                        {
                            tp = "t" + ObtenerIndice(variable, temp[i + 1].Texto);
                        }
                        else
                        {
                            tp = temp[i + 1].Texto;
                        }
                        if (temp[i].Texto == "+")
                        {
                            cadena = tn + " := " + tm + " + " + tp + "\r\n";
                        }
                        else if (temp[i].Texto == "-")
                        {
                            cadena = tn + " := " + tm + " - " + tp + "\r\n";
                        }
                        else if (temp[i].Texto == "*")
                        {
                            cadena = tn + " := " + tm + " * " + tp + "\r\n";
                        }
                        else
                        {
                            cadena = tn + " := " + tm + " / " + tp + "\r\n";
                        }
                        codigo.Add(cadena);
                        t++;
                    }
                }
                if (temp[i].TipoElemento == Enums.TipoElemento.OperadorRelacional)
                {
                    if (Verificar(ref variable, ref temp))
                    {
                        contador++;
                        String tn = "t" + (contador - ObtenerIndice(variable, temp[i - 1].Texto));
                        String tm = "t" + ObtenerIndice(variable, temp[i - 1].Texto);
                        String tp = "";
                        String cadena = "";
                        if (temp[i + 1].TipoElemento != Enums.TipoElemento.Numero)
                        {
                            tp = "t" + ObtenerIndice(variable, temp[i + 1].Texto);
                        }
                        else
                        {
                            tp = temp[i + 1].Texto;
                        }
                        if (temp[i].Texto == "<")
                        {
                            cadena = tn + " := " + tm + " < " + tp + "\r\n";
                        }
                        else if (temp[i].Texto == ">")
                        {
                            cadena = tn + " := " + tm + " > " + tp + "\r\n";
                        }
                        else if (temp[i].Texto == "<=")
                        {
                            cadena = tn + " := " + tm + " <= " + tp + "\r\n";
                        }
                        else if (temp[i].Texto == ">=")
                        {
                            cadena = tn + " := " + tm + " >= " + tp + "\r\n";
                        }
                        else if (temp[i].Texto == "==")
                        {
                            cadena = tn + " := " + tm + " == " + tp + "\r\n";
                        }
                        else if (temp[i].Texto == "!=")
                        {
                            cadena = tn + " := " + tm + " != " + tp + "\r\n";
                        }
                        codigo.Add(cadena);
                        t++;
                    }
                }

                if (temp[i].TipoElemento == Enums.TipoElemento.PalabraReservada)
                {
                    if (temp[i].Texto == "if" || temp[i].Texto == "while")
                    {
                        String tn = "t" + (bandera + 1);
                        String ti = "t" + (bandera + 2);
                        String cadena = "if " + tn + " goto " + ti + "\r\n";
                        codigo.Add(cadena);
                    }
                }

            }

            return codigo;
        }

        private bool Verificar(ref String[] elmento, ref Lexema[] temporal)
        {
            for (int i = 0; i < temporal.Length; i++)
            {
                foreach (var temp in elmento)
                {
                    if (temporal[i].Texto == temp)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        private int ObtenerIndice(String[] elemento, String texto)
        {
            for (int i = 0; i < elemento.Length; i++)
            {
                if (elemento[i] == texto)
                {
                    return i + 1;
                }
            }
            return 0;
        }

        private int ObtenerTotVariables(Lexema[] lex)
        {
            int cont = 0;
            for (int i = 0; i < lex.Length; i++)
            {
                if (lex[i].TipoElemento == Enums.TipoElemento.Variable)
                {
                    cont++;
                }
            }
            return cont;
        }
    }
}
