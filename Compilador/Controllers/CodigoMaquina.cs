﻿using System;
using System.Text;

namespace Compilador.Controllers
{
    class CodigoMaquina
    {
        public String GenerarCodigoMaquina(String entrada)
        {
            byte[] ba = Encoding.Default.GetBytes(entrada);
            String hexString = BitConverter.ToString(ba);
            hexString = hexString.Replace("-", "");
            return (CargarCabeceras() + hexString);
        }

        private String CargarCabeceras()
        {
            return "10 1kkk\t" +
                "kkkk kkkk\n" +
                "11 00xx\t" +
                "kkkk kkkk 0x5\n" +
                "11 111x\t" +
                "kkkk kkkk 0x4\n" +
                "00 0000\t" +
                "1fff ffff\n" +
                "00 0000\t" +
                "0xx0 0000\n";
        }
    }
}
