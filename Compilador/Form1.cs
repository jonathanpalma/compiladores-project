﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Compilador.Controllers;
using Compilador.Models;

namespace Compilador
{
    public partial class Form1 : Form
    {
        private readonly AnalizadorLexico _analizadorLexico;
        private readonly AnalizadorSintactico _analizadorSintactico;
        private readonly AnalizadorSemantico _analizadorSemantico;
        private readonly TablaSimbolos _tablaSimbolos;
        private readonly CodigoIntermedio _codinter;
        private readonly CodigoMaquina _codMaquina;
        OpenFileDialog openFile = new OpenFileDialog();
        SaveFileDialog saveFile = new SaveFileDialog();
     

        public Form1()
        {
            InitializeComponent();
            _analizadorLexico = new AnalizadorLexico();
            _analizadorSintactico = new AnalizadorSintactico();
            _analizadorSemantico = new AnalizadorSemantico();
            _tablaSimbolos = new TablaSimbolos();
            _codinter = new CodigoIntermedio();
            _codMaquina = new CodigoMaquina();

            openFile.Title = "Abrir archivos C";
            openFile.Filter = "Archivo C|*.c";
            openFile.InitialDirectory = @"C:\";
            saveFile.Title = "Guarcar archivo";
            saveFile.Filter = "Archivo C|*.c";
            saveFile.InitialDirectory = @"C:\";
        }
        
        private string ArmarResultadoAnalisisLexico(List<Lexema> lexemas)
        {
            string result = "";
            
            foreach (Lexema lexema in lexemas)
            {
                if (lexema.TipoElemento != Enums.TipoElemento.Error)
                {
                    result += "Lexema: ["+ lexema.Texto + "] del tipo: "+ lexema.TipoElemento + " \r\n";
                }
                else
                {
                    result += "Lexema: ["+ lexema.Texto + "] Error: "+ lexema.MensajeError + "  \r\n";
                }
                
            }
            return result;
        }

        private string ArmarErroresSintax(List<string> list)
        {
            string result = "Se detectaron los siguientes errores: \r\n";
            foreach (string error in list)
            {
                result = result + error + "\r\n";
            }

            return result;
        }

        public void LlenarArbol(ref TreeNode root, List<Bloque> bloquesTotales)
        {
            if (root == null)
            {
                root = new TreeNode();
                root.Text = "Programa";
                root.Tag = null;

                var bloques = bloquesTotales.Where(t => t.BloquePadre == null);
                foreach (var bloque in bloques)
                {
                    var child = new TreeNode()
                    {
                        Text = string.Join(" ", bloque.Lexemas.Select(y => y.Texto)),
                        Tag = bloque.Incia
                    };
                    LlenarArbol(ref child, bloquesTotales);
                    root.Nodes.Add(child);
                }
            }
            else
            {
                var id = (int)root.Tag;
                var bloques = bloquesTotales.Where(t => t.BloquePadre != null && t.BloquePadre.Incia == id);
                foreach (var bloque in bloques)
                {
                    var child = new TreeNode()
                    {
                        Text = string.Join(" ", bloque.Lexemas.Select(y => y.Texto)),
                        Tag = bloque.Incia
                    };
                    LlenarArbol(ref child, bloquesTotales);
                    root.Nodes.Add(child);
                }
            }
        }

        private void ImprimirTablaSimbolos(List<RegistroTabla> registros)
        {
            foreach (RegistroTabla registroTabla in registros)
            {
                int rowId = tablaSimbolos.Rows.Add();

                DataGridViewRow row = tablaSimbolos.Rows[rowId];

                row.Cells["Codigo"].Value = registroTabla.Codigo;
                row.Cells["Variable"].Value = registroTabla.Nombre;
                row.Cells["Tipo"].Value = registroTabla.TipoVariable;
                row.Cells["Valor"].Value = registroTabla.Valor;
            }
        }

        private void LimpiarTablaSimbolos()
        {
            tablaSimbolos.Rows.Clear();
        }

        private void NuevoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            cuadroTexto.Enabled = true;
            cuadroTexto.Text = null;
            cuadroLexico.Text = null;
            cuadroTexto.Focus();
        }

        private void AbrirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Stream myStream = null;

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = openFile.OpenFile()) != null)
                    {
                        using (myStream)
                        {
                            using (StreamReader r = new StreamReader(myStream))
                            {
                                string texto = r.ReadToEnd();
                                cuadroTexto.Text = texto;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error al leer el archivo: " + ex.Message);
                }
            }
        }

        private void GuardarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (saveFile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string path = saveFile.FileName;
                using (StreamWriter bw = new StreamWriter(File.Create(path)))
                {
                    bw.Write(cuadroTexto.Text);
                    bw.Close();
                }
            }
        }

        private void LexicoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnalisisLexico();
        }

        private void SintacticoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnalisisSintactico();
        }

        private void SemanticoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnalisisSemantico();
        }

        private void CodigoIntermedioToolStripMenuItem_Click(object sender, EventArgs e)
        {
            GenerarCodigoIntermedio();
        }

        private void CodigoMaquinaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string entrada = cuadroTexto.Text;
            string codMaquina = _codMaquina.GenerarCodigoMaquina(entrada);
            var result = MessageBox.Show(codMaquina, "Codigo Maquina",
                                         MessageBoxButtons.OK,
                                         MessageBoxIcon.Information);
        }

        private void AcercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            const string message = "William Alvarado\nJonathan Palma\nAlejandro Paz";
            const string caption = "Autores";
            var result = MessageBox.Show(message, caption,
                                         MessageBoxButtons.OK,
                                         MessageBoxIcon.Information);
        }

        private void LimpiarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            LimpiarTodo();
            cuadroTexto.Focus();
        }

        private void LimpiarTodo()
        {
            cuadroTexto.Clear();
            cuadroLexico.Clear();
            cuadroSintactico.Clear();
            cuadroSemantico.Clear();
            cuadroCodigoIntermedio.Clear();

            LimpiarTablaSimbolos();
        }

        private void AnalisisLexico()
        {
            string codigo = cuadroTexto.Text;
            codigo += "\r\n";
            string codigoSinComent = _analizadorLexico.RetirarComentarios(codigo);
            string codigoSinSaltos = _analizadorLexico.RetirarSaltos(codigoSinComent);
            List<Lexema> lexemas = _analizadorLexico.ExtraerLexemas(codigoSinSaltos);
            string analisisLex = ArmarResultadoAnalisisLexico(lexemas);
            cuadroLexico.Text = analisisLex;
        }

        private void AnalisisSintactico()
        {
            string codigo = cuadroTexto.Text;
            codigo += "\r\n";
            string codigoSinComent = _analizadorLexico.RetirarComentarios(codigo);
            string codigoSinSaltos = _analizadorLexico.RetirarSaltos(codigoSinComent);
            List<Lexema> lexemas = _analizadorLexico.ExtraerLexemas(codigoSinSaltos);

            _analizadorSintactico.ConstruirTablaSimbolos(lexemas);

            int pos = 0;

            List<Bloque> bloques = _analizadorSintactico.RealizarAnalisisSintax(lexemas, ref pos, lexemas.Count);
            bloques.ForEach(y => y.HacersePadre());
            List<Bloque> bloquesFlat = bloques.SelectMany(y => y.BloquesPlanos()).ToList();


            LimpiarTablaSimbolos();
            ImprimirTablaSimbolos(_analizadorSintactico.TablaSimbolos.RegistrosTabla);
            List<string> errores = _analizadorSintactico.ExtraerErroresBloques(bloques);
            if (errores.Count > 0)
            {
                arbolSintax.Nodes.Clear();
                cuadroSintactico.Text = ArmarErroresSintax(errores);
            }
            else
            {
                cuadroSintactico.Text = "Análisis sintactico correcto!";
                TreeNode root = null;
                arbolSintax.Nodes.Clear();
                LlenarArbol(ref root, bloquesFlat);
                arbolSintax.Nodes.Add(root);
            }
        }

        private void AnalisisSemantico()
        {
            string codigo = cuadroTexto.Text;
            codigo += "\r\n";
            string codigoSinComent = _analizadorLexico.RetirarComentarios(codigo);
            string codigoSinSaltos = _analizadorLexico.RetirarSaltos(codigoSinComent);
            List<Lexema> lexemas = _analizadorLexico.ExtraerLexemas(codigoSinSaltos);

            int pos = 0;


            List<Bloque> bloques = _analizadorSintactico.RealizarAnalisisSintax(lexemas, ref pos, lexemas.Count);
            bloques.ForEach(y => y.HacersePadre());
            List<Bloque> bloquesFlat = bloques.SelectMany(y => y.BloquesPlanos()).ToList();


            List<string> errores = _analizadorSintactico.ExtraerErroresBloques(bloques);
            if (errores.Count > 0)
            {
                arbolSintax.Nodes.Clear();
                cuadroSemantico.Text = ArmarErroresSintax(errores);
            }
            else
            {
                TreeNode root = null;
                arbolSintax.Nodes.Clear();
                LlenarArbol(ref root, bloquesFlat);
                arbolSintax.Nodes.Add(root);

                _analizadorSemantico.ProcesarLexemas(lexemas, bloques);

                if (_analizadorSemantico.Errores.Count > 0)
                {
                    string erroresSemantic = ArmarErroresSintax(_analizadorSemantico.Errores);
                    cuadroSemantico.Text = erroresSemantic;

                }
                else
                {
                    cuadroSemantico.Text = "Analisis semantico correcto";
                }

                LimpiarTablaSimbolos();
                ImprimirTablaSimbolos(_analizadorSemantico.TablaSimbolos.RegistrosTabla);

            }
        }

        private void GenerarCodigoIntermedio()
        {
            string codigo = cuadroTexto.Text;
            codigo += "\r\n";
            cuadroCodigoIntermedio.Text = "";
            string codigoSinComent = _analizadorLexico.RetirarComentarios(codigo);
            string codigoSinSaltos = _analizadorLexico.RetirarSaltos(codigoSinComent);
            List<Lexema> lexemas = _analizadorLexico.ExtraerLexemas(codigoSinSaltos);
            _codinter.ConstruirTabla(lexemas);
            List<String> prueba = _codinter.Genera_codigo(lexemas);
            foreach (var texto in prueba)
            {
                cuadroCodigoIntermedio.Text += texto + "\n";
            }
        }

        private void CompilarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AnalisisLexico();
            AnalisisSintactico();
            AnalisisSemantico();
            GenerarCodigoIntermedio();
        }
    }
}
