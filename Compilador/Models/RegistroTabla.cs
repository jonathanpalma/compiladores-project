﻿namespace Compilador.Models
{
    public class RegistroTabla
    {

        public int Codigo { get; set; }

        public string Nombre { get; set; }

        public Enums.TipoVariable? TipoVariable { get; set; }

        public string Valor { get; set; }

    }
}
