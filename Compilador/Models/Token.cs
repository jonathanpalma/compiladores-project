﻿namespace Compilador.Models
{
    public class Token
    {
        public string NombreToken { get; set; }
        
        public int? IdTablaSimbolos { get; set; }
        
        public Enums.TipoElemento TipoElemento { get; set; }
        
    }
}
